package Iwona.pl;


import java.util.List;

public class App {
    public static void main(String[] args) {

        MusicCollection musicCollection = new MusicCollection();

        List<Song> songs = musicCollection.createStream();
        int time = musicCollection.timeAllSongsForGenre(songs, Song.Genre.POP);
        System.out.println("Łączny czas piosenek popowych na playliście (w sekundach): " + time);

        long author = musicCollection.numberOfSongsForAuthor(songs, "Alicia Keys");
        System.out.println("Liczba piosenek artysty na playliście: " + author);

        List<Song> withoutGenre = musicCollection.litOfSongs(songs, Song.Genre.POP);
        withoutGenre.forEach(System.out::println);

    }
}
