package Iwona.pl;

import java.util.List;
import java.util.stream.Collectors;

public class MusicCollection {

    public List<Song> createStream() {
        return List.of(
                new Song("Nothing Else Matters", 386, "Metallica", Song.Genre.METAL),
                new Song("No One", 248, "Alicia Keys", Song.Genre.POP),
                new Song("Nothing Else Matters", 386, "Metallica", Song.Genre.METAL),
                new Song("Believer", 216, "Imagine Dragons", Song.Genre.ROCK),
                new Song("Fear of The Dark", 438, "Iron Maiden", Song.Genre.METAL),
                new Song("Enter Sandman", 346, "Metallica", Song.Genre.METAL),
                new Song("The Unforgiven", 348, "Metallica", Song.Genre.METAL),
                new Song("Girl on Fire", 404, "Alicia Keys", Song.Genre.POP)
        );
    }

    //    metoda przyjmuje listę utworów i gatunek muzyczny, a zwraca łączny czas trwania wszystkich utworów danego gatunku,
    public int timeAllSongsForGenre(List<Song> list, Song.Genre genre) {
        return list.stream().filter(song -> song.getGenre() == genre)
                .mapToInt(Song::getLength)
                .sum();
    }

    //    metoda przyjmuje listę utworów i nazwę artysty, a zwraca liczbę utworów danego autora w tej liście,
    public long numberOfSongsForAuthor(List<Song> list, String artist) {
        return list.stream().filter(song -> song.getArtist().equalsIgnoreCase(artist))
                .count();
    }

//    metoda przyjmuje listę utworów i gatunek muzyczny, a zwraca listę utworów bez utworów,
//    które miały przypisany ten gatunek muzyczny oraz bez duplikatów (powtarzających się utworów).

    public List<Song> litOfSongs(List<Song> list, Song.Genre genreToRemove) {
      return  list.stream().filter(song -> song.getGenre() != genreToRemove)
                .distinct()
                .collect(Collectors.toList());
    }

}
